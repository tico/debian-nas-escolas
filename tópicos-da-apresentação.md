
# Debian nas escolas

**Projeto para divulgação do software livre e do Debian GNU/Linux nas escolas de
Ensino Médio.**

[es] Proyecto de la comunidad brasileña para promover Debian GNU/Linux en las
escuelas secundarias.

[en] Brazilian community project to promote Debian GNU/Linux in high schools.

## Proposta de tópicos para a apresentação nas escolas

Esta é uma proposta genérica, servindo como ponto de partida para outras possibilidades.
A apresentação tem como ponto final a exposição do Projeto Debian, como comunidade e
como sistema operacional, após uma breve descoberta de aspectos tecnológicos e suas
relações históricas, sociais e políticas.

Cada etapa tem seus *objetivos* e *conceitos*, vinculando o que se apresenta com
o que é definido pela BNCC. Além disso, nas entradas *BNCC* indica-se as competências
que estão sendo trabalhadas no tópico. Isto também permite traçar diálogos específicos
com o/a professor/a da aula, de acordo com sua disciplina.

**Visão geral do percurso**: o encontro se inicia com a acolhida dos/as estudantes
e o incentivo para conhecer o tema. A seguir, apresenta-se o conceito de hardware
e software, culminando na apresentação do sistema operacional como software responsável
pela gerência do hardware. Passa-se para a problematização de software livre e software
proprietário/privativo, e posterior movimentos sociais que dali surgiram. Por fim,
chega-se ao Debian GNU/Linux como sistema operacional e como comunidade. Termina-se
com um chamado à utilização e à contribuição ao software livre.

 0. Boas-vindas e Introdução

  + *Objetivos*: Recepção e acolhida. Levantar interesse dos/as estudantes.
  + *Conceitos*: Estranhamento. Curiosidade. Investigação sistemática e análise.
  + *BNCC*     : CHS6 – EM13CHS102

 1. Hardware

  + *Objetivos*: Apresentar o conceito de hardware. Explicar a geração de informação a partir de bits e bytes.
  + *Conceitos*: Arquitetura de Von Neumann. CPU, MMU, memórias, barramentos. Periféricos de E/S. Bits e bytes.
  + *BNCC*     : CNT3 – EM13CNT308

 2. Software

  + *Objetivos*: Apresentar o conceito de software e de lógica algoritmica.
  + *Conceitos*: Software. Lógica algorítmica. Funções, bibliotecas compartilhadas, módulos.
  + *BNCC*     : MAT4 – EM13MAT310; MAT4 – EM13MAT315

 3. Sistema operacional

  + *Objetivos*: Apresentar a função do software "sistema operacional".
  + *Conceitos*: Abstrações e gerência de recursos. Núcleo (kernel) e aplicações centrais.
  + *BNCC*     : LGG7 – EM13LGG104

 4. Software livre e software proprietário/privativo

  + *Objetivos*: Articular a construção de softwares com a sociedade de mercado. Apresentar os conceitos de software livre e proprietário ou privado.
  + *Conceitos*: Sociedade de mercado e mercadoria. Software proprietário e conhecimento privado. Software livre e liberdades.
  + *BNCC*     : CHS1 – EM13CHS401; LGG4 – EM13LGG102

 5. Movimento Software Livre

  + *Objetivos*: Apresentar o movimento do software livre. Apresentar o embate conceitual e ético entre FSF e OSI.
  + *Conceitos*: Movimentos sociais. Embate ideológico. Licenciamento.
  + *BNCC*     : CHS1 – EM13CHS401; LGG2 – EM13LGG302

 6. Debian GNU/Linux

  + *Objetivos*: Apresentar o Debian GNU/Linux como sistema operacional.
  + *Conceitos*: Sistema operacional. Núcleo (kernel) e aplicações centrais. Distribuição.
  + *BNCC*     : CHS3 – EM13CHS301

 7. Comunidade Debian

  + *Objetivos*: Apresentar o Debian GNU/Linux como comunidade de prática.
  + *Conceitos*: Comunidade de prática. Construção colaborativa. Projeto de vida.
  + *BNCC*     : LGG5 – EM13LGG202; CG2

 8. Encerramento

  + *Objetivos*: Enfatizar o uso de softwares livres e a possibilidade de contribuição. Dirimir questionamentos.
  + *Conceitos*: O ‘uso’ como modalidade de contribução. Reflexão sobre demandas pessoais
  + *BNCC*     : CG2
