# Debian nas escolas

**Projeto para divulgação do software livre e do Debian GNU/Linux nas escolas de
Ensino Médio.**

[es] Proyecto de la comunidad brasileña para promover Debian GNU/Linux en las
escuelas secundarias.

[en] Brazilian community project to promote Debian GNU/Linux in high schools.

Este é o **repositório** de recursos para utilização compartilhada pelos/as
participantes.

A primeira formulação do projeto e os encaminhamentos preliminares encontram-se
[nesta página do wiki](https://wiki.debian.org/Brasil/DebianNasEscolas).

Entre em contato através do [grupo DebianEduBr no Telegram](https://t.me/DebianEduBR).

----

# Licenciamento CC-BY-4.0

![](ccby.png)
Esta licença é aceitável para Trabalhos Culturais Livres.
