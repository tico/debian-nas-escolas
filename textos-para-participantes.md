# Debian nas escolas

**Projeto para divulgação do software livre e do Debian GNU/Linux nas escolas de
Ensino Médio.**

[es] Proyecto de la comunidad brasileña para promover Debian GNU/Linux en las
escuelas secundarias.

[en] Brazilian community project to promote Debian GNU/Linux in high schools.

## Textos essenciais para participantes

O objetivo desta lista é alinhar informações essenciais entre quem realizará as
apresentações nas escolas. São ideias e conceitos que respaldam o posicionamento
da comunidade Debian, bem como abrem canais de diálogo com o contexto escolar.

Panoramicamente, os materiais abordam:

 1. Princípios de atuação da comunidade Debian;
 2. Uma visão geral da Base Nacional Comum Curricular, do novo Ensino Médio, e de
suas relações com as tecnologias.
 3. Uma visão geral do movimento Software Livre, de hardware e de sistemas operacionais.
 4. Ideias iniciais sobre as relações entre Educação, Debian e Software Livre.

### O que é preciso fazer?

 * Para cada texto, um pequeno comentário sobre a importância de se conhecer
determinado documento em relação às apresentações nas escolas.

 * No tema "Hardware e sistemas operacionais", falta um texto mais didático
sobre arquiteturas de hardware.

### Debian

PROJETO DEBIAN. **Contrato Social Debian**.
Disponível em: <https://www.debian.org/social_contract.pt.html>.

PROJETO DEBIAN. **Código de conduta Debian**.
Disponível em: <https://www.debian.org/code_of_conduct.pt.html>.

PROJETO DEBIAN. **Declaração de diversidade**.
Disponível em: <https://www.debian.org/intro/diversity.pt.html>.

PROJETO DEBIAN. **Uma Breve História da Debian**.
Disponível em: <https://www.debian.org/doc/manuals/project-history/index.pt.html>.

### Software livre e Software proprietário/privado

PROJETO DEBIAN. **O que significa livre?**
Disponível em: <https://www.debian.org/intro/free.pt.html>.

FREE SOFTWARE FOUNDATION. **O que é software livre?**
Disponível em: <https://www.gnu.org/philosophy/free-sw.pt-br.html>.

### Novo Ensino Médio

BRASIL. MEC. **Base Nacional Comum Curricular**. Brasília: Ministério da Educação, 2017.
Disponível em: <http://basenacionalcomum.mec.gov.br>.

*Comentário*: Não é necessária a leitura completa do documento, apenas as seções que
tratam das especificações mais gerais da proposta pedagógica no Ensino Médio.

 * Introdução e 10 Competências gerais da Educação Básica, p. 7-10.
 * Capítulo 5 - A Etapa do Ensino Médio, p. 461-479.

### Educação, software livre e tecnologias

BRASIL. MEC. **Base Nacional Comum Curricular**. Brasília: Ministério da Educação, 2017.
Disponível em: <http://basenacionalcomum.mec.gov.br>.

*Comentário*: É importante conhecer a seção "As tecnologias digitais e a computação", p. 473-475.

DEBIAN BRASIL. **A professora e o Debian**.
Disponível em: <https://www.youtube.com/watch?v=D-S5WuMW8Fc>.

*Comentário*: apresentação da Virgínia Fernandes, professora de Educação Fundamental
da Rede Pública de Educação Básica do Distrito Federal.

DEBXP. **O professor de Debianópolis**: trabalhando com softwares livres nas escolas.
Disponível em: <https://www.youtube.com/watch?v=QYgveO7KUUM>.

*Comentário*: apresentação do [Ramon Mulin](https://mulin.codeberg.page/), professor
de História da Rede Pública de Educação Básica do Rio de Janeiro. O Mulin é a referência
mais atuante quando se trata de relações entre Educação e comunidade Debian.

FREE SOFTWARE FOUNDATION. **Software Livre e Educação**.
Disponível em <https://www.gnu.org/education/education.pt-br.html>.

*Comentário*: Há um vídeo do Stallman bem explicativo nesta página.

WING, J. **Pensamento computacional**: um conjunto de atitudes e habilidades que
todos, não só cientistas da computação, ficaram ansiosos para aprender e usar.
R. bras. Ens. Ci. Tecnol., Ponta Grossa, v. 9, n. 2, p. 1-10, mai./ago. 2016.

### Hardware e sistemas operacionais

MAZIERO, C. Conceitos básicos. In: ______. **Sistemas Operacionais**: Conceitos e Mecanismos.
Editora da UFPR, 2019. p. 2-12.
Disponível em: <http://wiki.inf.ufpr.br/maziero/lib/exe/fetch.php?media=socm:socm-01.pdf>.
